import networkx as nx


if __name__ == "__main__":
    G = nx.DiGraph()

    G.add_edge('base_plate', 'module_1')
    G.add_edge('cab_lower', 'module_1')
    G.add_edge('cab_upper', 'module_2')
    G.add_edge('module_1', 'module_2')
    G.add_edge('boiler', 'module_3')
    G.add_edge('module_2', 'module_3')
    G.add_edge('tire_1', 'module_4')
    G.add_edge('module_3', 'module_4')
    G.add_edge('tire_2', 'module_5')
    G.add_edge('module_4', 'module_5')
    G.add_edge('tire_3', 'module_6')
    G.add_edge('module_5', 'module_6')
    G.add_edge('tire_4', 'module_7')
    G.add_edge('module_6', 'module_7')

    nx.write_gpickle(G, 'module_graph_assembly_3.gpickle')
