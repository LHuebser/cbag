from matplotlib import pyplot as plt
import numpy as np
import imageio
import argparse
import time
import cv2 as cv

from cbag.visualization import zeroing
from cbag.visualization.stl_mesh import draw_meshes, move_meshes
from cbag.visualization.visualize import get_img_from_fig
from cbag import utils


def main(sheet_name, stop_row, interpol_factor, save_show, save_show_name, resolution, frames_per_second):
    poses = utils.load_from_xls(utils.locomotive_data_pth, sheet_name, stop_row,
                                utils.locomotive_data_fields, interpol_factor)
    parts = utils.locomotive_parts
    meshes = zeroing.load_from_stl()

    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')
    plt.ion()
    plt.show()

    imgs = []

    for t_step in range(poses.shape[0]):
        meshes = move_meshes(meshes, parts, poses, t_step)

        draw_meshes(meshes, ax)

        ax.set_xlim3d(-400, 300)
        ax.set_ylim3d(-200, 400)
        ax.set_zlim3d(-10, 400)

        max_range = 700
        Xb = 0.5 * max_range * np.mgrid[-1:2:2, -1:2:2, -1:2:2][0].flatten() + 0.5 * (300 + -400)
        Yb = 0.5 * max_range * np.mgrid[-1:2:2, -1:2:2, -1:2:2][1].flatten() + 0.5 * (400 + -200)
        Zb = 0.5 * max_range * np.mgrid[-1:2:2, -1:2:2, -1:2:2][2].flatten() + 0.5 * (400 + -10)
        for xb, yb, zb in zip(Xb, Yb, Zb):
            ax.plot([xb], [yb], [zb], 'w')

        img = get_img_from_fig(fig, resolution) if save_show else None
        imgs.append(img)

        if t_step in [0, 10, 20, 30, 40, 50]:
            img = get_img_from_fig(fig, resolution)
            cv.imwrite(f'../doc/scene_visualization/{save_show_name}_{t_step}.png', img)



        plt.draw()
        plt.pause(0.01)
        ax.cla()
    time.sleep(0.25)

    if save_show:
        writer = imageio.get_writer(f'../doc/scene_visualization/{save_show_name}.mp4', fps=frames_per_second)
        for im in imgs:
            writer.append_data(im)
        writer.close()


if __name__ == "__main__":
    # sheet:    Montage1  Montage2    Montage3    Montage4      Montage5   Montage6   Montage7
    # stop row: 70        45          50          55            66         63         55
    parser = argparse.ArgumentParser()
    parser.add_argument('-sn', '--sheet_name', action='store', type=str, default='Montage7')
    parser.add_argument('-sr', '--stop_row', action='store', type=int, default=55)
    parser.add_argument('-if', '--interpol_factor', action='store', type=int, default=1)
    parser.add_argument('-sv', '--save_show', action='store_true', default=True)
    parser.add_argument('-svn', '--save_show_name', action='store', type=str, default='assembly_7')
    parser.add_argument('-r', '--resolution', action='store', type=int, default=180)
    parser.add_argument('-fps', '--frames_per_second', action='store', type=int, default=1)

    args = parser.parse_args()

    main(args.sheet_name, args.stop_row, args.interpol_factor, args.save_show, args.save_show_name, args.resolution,
         args.frames_per_second)
