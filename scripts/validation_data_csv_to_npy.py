import pandas as pd
import numpy as np


assembly_name = 'assembly_5'


# todo: add argparse
if __name__ == "__main__":
    data = pd.read_csv(f'../doc/scene_annotation/{assembly_name}_mtm_left.csv',
                       header=None, delimiter='\n').to_numpy()
    new_data = []
    for d in data:
        try:
            new_data.append(int(d[0]))
        except Exception:
            new_data.append(np.array(d[0].replace('[', '').replace(']', '').replace("'", '').split(','), dtype=np.int))
    data = np.array(new_data)
    swimlane = np.zeros((data.shape[0], 6))
    for i, d in enumerate(data):
        if type(d) == int:
            swimlane[i, d - 1] = d
        else:
            for val in d:
                swimlane[i, val-1] = val
    np.save(f'../doc/scene_annotation/{assembly_name}_mtm_left.npy', swimlane)

    data = pd.read_csv(f'../doc/scene_annotation/{assembly_name}_mtm_right.csv',
                       header=None, delimiter='\n').to_numpy()
    new_data = []
    for d in data:
        try:
            new_data.append(int(d[0]))
        except Exception:
            new_data.append(np.array(d[0].replace('[', '').replace(']', '').replace("'", '').split(','), dtype=np.int))
    data = np.array(new_data)
    swimlane = np.zeros((data.shape[0], 6))
    for i, d in enumerate(data):
        if type(d) == int:
            swimlane[i, d - 1] = d
        else:
            for val in d:
                swimlane[i, val - 1] = val
    np.save(f'../doc/scene_annotation/{assembly_name}_mtm_right.npy', swimlane)
