from matplotlib import pyplot as plt
from matplotlib import gridspec
import numpy as np
import imageio
import argparse
import time
import networkx as nx
import cv2 as cv
# noinspection PyProtectedMember
from matplotlib.axes._axes import _log as matplotlib_axes_logger

from cbag import core
from cbag.inference import assembly as asm
from cbag.visualization import zeroing, visualize
from cbag.visualization.stl_mesh import draw_meshes, move_meshes
from cbag.visualization.visualize import get_img_from_fig
from cbag import utils


matplotlib_axes_logger.setLevel('ERROR')


def main(sheet_name, stop_row, interpol_factor, save_show, save_show_name, resolution, frames_per_second):
    poses = utils.load_from_xls(utils.locomotive_data_pth, sheet_name, stop_row,
                                utils.locomotive_data_fields, interpol_factor)
    parts = utils.locomotive_parts
    meshes = zeroing.load_from_stl()
    eps = utils.locomotive_epsilon

    D_star = core.distance_matrix(np.array([poses[-1]]))
    D_star = D_star[-1, :, :]
    D_star = asm.assembly_connectivity(D_star, utils.locomotive_connectivity)
    assembly_status, _, assembly_graph, _, _, _, S_adj = \
        asm.inference_step(poses, parts, D_star, eps['epsilon_D_star'], eps['epsilon_V_rel_min'],
                           eps['epsilon_V_rel_max'], eps['epsilon_dP'], zero_vel_assumption=True, only_workpieces=True)
    instructions = asm.generate_simple_instructions(assembly_status, assembly_graph)
    node_positions = visualize.node_position_workpiece_graph(assembly_graph)

    print(D.shape, assembly_status.shape, poses.shape, S_adj.shape)

    fig = plt.figure()
    fig.tight_layout()
    gs = gridspec.GridSpec(1, 5, left=0, right=0.98, top=0.75, bottom=0.05, wspace=0.4)
    ax0 = fig.add_subplot(gs[0, :2], projection='3d')
    ax1 = fig.add_subplot(gs[0, 2:4])
    ax2 = fig.add_subplot(gs[0, 4])
    # ax1.axis('off')
    ax2.axis('off')
    # ax3.set_xticks([])
    # ax3.set_yticks([])
    ax = [ax0, ax1, ax2]

    fig.canvas.manager.full_screen_toggle()

    plt.ion()
    plt.show()

    imgs = []
    draw_nodes = []

    max_range = 700
    for t_step in range(poses.shape[0]):
        meshes = move_meshes(meshes, parts, poses, t_step)

        plt.suptitle('Working Principle of the Inference Algorithm', fontweight='bold')
        plt.sca(ax[0])
        draw_meshes(meshes, ax[0])
        ax[0].set_xlim3d(-400, 300)
        ax[0].set_ylim3d(-400, 300)
        ax[0].set_zlim3d(-10, 600)
        Xb = 0.5 * max_range * np.mgrid[-1:2:2, -1:2:2, -1:2:2][0].flatten() + 0.5 * (300 + -400)
        Yb = 0.5 * max_range * np.mgrid[-1:2:2, -1:2:2, -1:2:2][1].flatten() + 0.5 * (300 + -400)
        Zb = 0.5 * max_range * np.mgrid[-1:2:2, -1:2:2, -1:2:2][2].flatten() + 0.5 * (600 + -10)
        for xb, yb, zb in zip(Xb, Yb, Zb):
            ax[0].plot([xb], [yb], [zb], 'w')
        plt.title('Synthetic assembly scene', fontweight='bold')

        plt.sca(ax[1])
        ax[1].imshow(S_adj[t_step, 2:, 2:], cmap='jet', aspect='auto', interpolation='nearest')
        ax[1].set_xticks(np.arange(len(parts[2:])))
        ax[1].set_yticks(np.arange(len(parts[2:])))
        ax[1].set_xticklabels(parts[2:], rotation=80)
        ax[1].set_yticklabels(parts[2:], rotation=10)
        plt.tick_params(axis='both', which='major', labelsize=10, labelbottom=False,
                        bottom=False, top=False, labeltop=True)
        ax[1].set_xticks(np.arange(len(parts[2:])+1) - 0.5, minor=True)
        ax[1].set_yticks(np.arange(len(parts[2:])+1) - 0.5, minor=True)
        # plt.ylabel('assembly parts')
        plt.xlabel('assembly parts')
        plt.title('Adjacency matrix of assembled parts', fontweight='bold')

        plt.sca(ax[2])
        if instructions_at_t := instructions[t_step]:
            draw_nodes = []
            for instr in instructions_at_t:
                instr = instr.replace('assemble ', '')
                instr = instr.replace('to ', '')
                instr = instr.replace('disassemble ', '')
                instr = instr.replace('from ', '')
                draw_nodes.extend(instr.split(' '))
        residual_nodes = [node for node in list(assembly_graph.nodes()) if node not in draw_nodes]
        nx.draw(assembly_graph, pos=node_positions, nodelist=residual_nodes, node_color=(0.82, 0.83, 0.84))
        nx.draw(assembly_graph, pos=node_positions, nodelist=draw_nodes, node_color=(1, 0.549, 0), with_labels=True)
        plt.title('Assembly module graph', fontweight='bold')

        img = get_img_from_fig(fig, resolution) if save_show else None
        imgs.append(img)

        time.sleep(0.01)
        plt.draw()
        plt.pause(0.01)
        ax[0].cla()
        ax[1].cla()
        ax[2].cla()
    time.sleep(0.2)
    # noinspection PyUnboundLocalVariable
    imgs.extend(img for _ in range(5))

    if save_show:
        shape_out = np.argmax([i.shape[0] for i in imgs])
        shape_out = imgs[int(shape_out)].shape
        # hard coded fix...
        if (shape_out[0] == 2988) and (shape_out[1] == 5529):
            shape_out = (2992, 5536, 3)
        writer = imageio.get_writer(f'../doc/{save_show_name}.mp4', fps=frames_per_second)
        for im in imgs:
            im = cv.resize(im, (shape_out[1], shape_out[0]))
            writer.append_data(im)
        writer.close()


if __name__ == "__main__":
    # sheet:    Montage1  Montage2    Montage3    Montage4
    # stop row: 70        45          50          55
    parser = argparse.ArgumentParser()
    parser.add_argument('-sn', '--sheet_name', action='store', type=str, default='Montage4')
    parser.add_argument('-sr', '--stop_row', action='store', type=int, default=55)
    parser.add_argument('-if', '--interpol_factor', action='store', type=int, default=2)
    parser.add_argument('-sv', '--save_show', action='store_true', default=False)
    parser.add_argument('-svn', '--save_show_name', action='store', type=str, default='cornelius_adjacency_4')
    parser.add_argument('-r', '--resolution', action='store', type=int, default=180)
    parser.add_argument('-fps', '--frames_per_second', action='store', type=int, default=3)

    args = parser.parse_args()

    main(args.sheet_name, args.stop_row, args.interpol_factor, args.save_show, args.save_show_name, args.resolution,
         args.frames_per_second)
