import cv2 as cv
import csv
from matplotlib import pyplot as plt


assembly_name = 'assembly_5'    # set accordingly


# todo: add argparse
if __name__ == "__main__":
    video_name = f'../doc/scene_visualization/{assembly_name}.mp4'
    cap = cv.VideoCapture(video_name)

    labels = []
    mtm = {0: 'D', 1: 'R', 2: 'G', 3: 'M', 4: 'P', 5: 'DP', 6: 'RL'}
    frames = []

    while cap.isOpened():
        ret, frame = cap.read()
        if ret is True:
            frames.append(frame)
        else:
            break
    cap.release()
    cv.destroyAllWindows()
    print('Frames: ', len(frames))

    i = 0
    plt.figure()
    while i < len(frames):
        frame = frames[i]
        plt.imshow(frame)
        plt.pause(0.1)
        annotation = input('mtm: ')
        try:
            if annotation == 'b':
                i -= 1
                del labels[-1]
                print('rewind')
            else:
                annotations = annotation.split(',')
                print([mtm[int(a)] for a in annotations], len(labels), i)
                labels.append(annotations)
                i += 1
        except Exception:
            print('wrong input')

    with open(f'../doc/scene_annotation/{assembly_name}_mtm_right.csv', 'w', newline='') as f:
        wr = csv.writer(f, delimiter="\n")
        wr.writerow(labels)

    print('Done')
