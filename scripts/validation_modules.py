import networkx as nx
import numpy as np
from matplotlib import pyplot as plt

from cbag import core
from cbag.inference import assembly as asm
from cbag.visualization import zeroing, visualize
from cbag import utils

sheet_name = 'Montage6'
stop_row = 63
interpol_factor = 1
assembly_name = 'assembly_6'


# sheet:    Montage1  Montage2    Montage3    Montage4      Montage5  Montage6
# stop row: 70        45          50          55            66        63
# todo: add argparse
if __name__ == "__main__":
    poses = utils.load_from_xls(utils.locomotive_data_pth, sheet_name, stop_row,
                                utils.locomotive_data_fields, interpol_factor)
    parts = utils.locomotive_parts
    meshes = zeroing.load_from_stl()
    eps = utils.locomotive_epsilon

    D_star = core.distance_matrix(np.array([poses[-1]]))
    D_star = D_star[-1, :, :]
    D_star = asm.assembly_connectivity(D_star, utils.locomotive_connectivity)
    assembly_status, _, assembly_graph, _, _, _, S_adj = \
        asm.inference_step(poses, parts, D_star, eps['epsilon_D_star'], eps['epsilon_V_rel_min'],
                           eps['epsilon_V_rel_max'], eps['epsilon_dP'], zero_vel_assumption=True, only_workpieces=True)
    node_positions = visualize.node_position_workpiece_graph(assembly_graph)
    plt.figure()
    nx.draw(assembly_graph, pos=node_positions, node_color=(0.82, 0.83, 0.84), with_labels=True)
    plt.savefig(f'../doc/validation/module_{assembly_name}.png')

    val_G = nx.read_gpickle(f'../doc/scene_annotation/module_graph_{assembly_name}.gpickle')
    node_positions = visualize.node_position_workpiece_graph(val_G)
    plt.figure()
    nx.draw(val_G, pos=node_positions, node_color=(0.82, 0.83, 0.84), with_labels=True)
    plt.savefig(f'../doc/validation/validation_module_{assembly_name}.png')

    plt.figure()
    plt.imshow(assembly_status, interpolation='nearest', aspect=0.25, vmin=0, vmax=10)
    plt.xticks(range(len(parts)), parts, rotation=80)
    plt.tick_params(axis='both', which='major', labelsize=10, labelbottom=False,
                    bottom=False, top=False, labeltop=True)
    plt.ylabel('time steps')
    plt.grid(True)
    plt.savefig(f'../doc/validation/validation_assembly_status_{assembly_name}.png')
    plt.show()
