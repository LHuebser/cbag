import numpy as np
from matplotlib import pyplot as plt
import matplotlib.patches as matplot_patches

from cbag.inference import mtm
from cbag import utils
from cbag.visualization import zeroing, visualize


sheet_name = 'Montage6'
stop_row = 63
interpol_factor = 1
assembly_name = 'assembly_6'


def to_super_state(arr):
    for i, a in enumerate(arr):
        if 3 in a:
            arr[i, 2] = 7
        if 4 in a:
            arr[i, 2] = 7
            arr[i, 3] = 0
        if 5 in a:
            arr[i, 2] = 7
            arr[i, 4] = 0
        if 2 in a:
            arr[i, 2] = 7
            arr[i, 1] = 0
    return arr


# sheet:    Montage1  Montage2    Montage3    Montage4      Montage5   Montage6
# stop row: 70        45          50          55            66         63
# todo: add argparse
# todo: save to /data instead to /doc
if __name__ == "__main__":
    poses = utils.load_from_xls(utils.locomotive_data_pth, sheet_name, stop_row,
                                utils.locomotive_data_fields, interpol_factor)
    parts = utils.locomotive_parts
    meshes = zeroing.load_from_stl()
    eps = utils.locomotive_epsilon
    mtm_ops = ['reach', 'grasp', 'move', 'position', 'de-position', 'release']
    mtm_states = ['default', 'reach', 'grasp', 'move', 'position', 'de-position', 'release']

    S_hl, S_hr, assembly_status, assembly_modules, assembly_graph = \
        mtm.inference_back_propagation(poses, parts, eps['epsilon_D_star'], eps['epsilon_V_rel_min'],
                                       eps['epsilon_V_rel_max'],
                                       eps['epsilon_dP'], eps['epsilon_V_abs'], utils.locomotive_connectivity,
                                       zero_vel_assumption=True, only_workpieces=True,
                                       unassembled_start_assumption=True)
    swimlane_left = mtm.mtm_graph_from_states(S_hl)
    swimlane_right = mtm.mtm_graph_from_states(S_hr)

    swimlane_left_val = np.load(f'../doc/scene_annotation/{assembly_name}_mtm_left.npy')
    swimlane_right_val = np.load(f'../doc/scene_annotation/{assembly_name}_mtm_right.npy')

    right_diff = np.zeros_like(swimlane_right)
    for i in range(swimlane_right.shape[0]):
        for j in range(swimlane_right.shape[1]):
            if swimlane_right[i, j] > swimlane_right_val[i, j]:
                right_diff[i, j] = -1
            elif swimlane_right[i, j] < swimlane_right_val[i, j]:
                right_diff[i, j] = 1

    left_diff = np.zeros_like(swimlane_left)
    for i in range(swimlane_left.shape[0]):
        for j in range(swimlane_left.shape[1]):
            if swimlane_left[i, j] > swimlane_left_val[i, j]:
                left_diff[i, j] = -1
            elif swimlane_left[i, j] < swimlane_left_val[i, j]:
                left_diff[i, j] = 1

    acc_left = 1-(np.count_nonzero(swimlane_left - swimlane_left_val)/swimlane_left_val.size)
    acc_right = 1-(np.count_nonzero(swimlane_right - swimlane_right_val)/swimlane_right_val.size)

    super_left = to_super_state(swimlane_left.copy())
    super_right = to_super_state(swimlane_right.copy())
    super_left_val = to_super_state(swimlane_left_val.copy())
    super_right_val = to_super_state(swimlane_right_val.copy())

    super_acc_left = 1 - (np.count_nonzero(super_left - super_left_val) / super_left_val.size)
    super_acc_right = 1 - (np.count_nonzero(super_right - super_right_val) / super_right_val.size)

    print(acc_left, acc_right, super_acc_left, super_acc_right)
    with open(f'../doc/validation/acc_{assembly_name}.txt', 'w') as file:
        file.write(f'acc_left: {acc_left}\n')
        file.write(f'acc_right: {acc_right}\n')
        file.write(f'super_acc_left: {super_acc_left}\n')
        file.write(f'super_acc_right: {super_acc_right}')
        file.close()

    plt.figure()
    plt.imshow(swimlane_left, interpolation='nearest', aspect='auto', vmin=-6, vmax=6)
    plt.xticks(range(len(mtm_ops)), mtm_ops, rotation=80)
    plt.tick_params(axis='both', which='major', labelsize=10, labelbottom=False,
                    bottom=False, top=False, labeltop=True)
    plt.ylabel('time steps')
    plt.grid(True)
    plt.savefig(f'../doc/validation/mtm_left_{assembly_name}.png')

    plt.figure()
    plt.imshow(swimlane_right, interpolation='nearest', aspect='auto', vmin=-6, vmax=6)
    plt.xticks(range(len(mtm_ops)), mtm_ops, rotation=80)
    plt.tick_params(axis='both', which='major', labelsize=10, labelbottom=False,
                    bottom=False, top=False, labeltop=True)
    plt.ylabel('time steps')
    plt.grid(True)
    plt.savefig(f'../doc/validation/mtm_right_{assembly_name}.png')

    plt.figure()
    plt.imshow(swimlane_left_val, interpolation='nearest', aspect='auto', vmin=-6, vmax=6)
    plt.xticks(range(len(mtm_ops)), mtm_ops, rotation=80)
    plt.tick_params(axis='both', which='major', labelsize=10, labelbottom=False,
                    bottom=False, top=False, labeltop=True)
    plt.ylabel('time steps')
    plt.grid(True)
    plt.savefig(f'../doc/validation/validation_mtm_left_{assembly_name}.png')

    plt.figure()
    plt.imshow(swimlane_right_val, interpolation='nearest', aspect='auto', vmin=-6, vmax=6)
    plt.xticks(range(len(mtm_ops)), mtm_ops, rotation=80)
    plt.tick_params(axis='both', which='major', labelsize=10, labelbottom=False,
                    bottom=False, top=False, labeltop=True)
    plt.ylabel('time steps')
    plt.grid(True)
    plt.savefig(f'../doc/validation/validation_mtm_right_{assembly_name}.png')

    plt.figure()
    plt.imshow(left_diff, interpolation='nearest', aspect='auto', vmin=-1, vmax=1)
    # plt.colorbar()
    plt.xticks(range(len(mtm_ops)), mtm_ops, rotation=80)
    plt.tick_params(axis='both', which='major', labelsize=10, labelbottom=False,
                    bottom=False, top=False, labeltop=True)
    plt.ylabel('time steps')
    plt.grid(True)
    plt.savefig(f'../doc/validation/diff_mtm_left_{assembly_name}.png')

    plt.figure()
    plt.imshow(right_diff, interpolation='nearest', aspect='auto', vmin=-1, vmax=1)
    # plt.colorbar()
    plt.xticks(range(len(mtm_ops)), mtm_ops, rotation=80)
    plt.tick_params(axis='both', which='major', labelsize=10, labelbottom=False,
                    bottom=False, top=False, labeltop=True)
    plt.ylabel('time steps')
    plt.grid(True)
    plt.savefig(f'../doc/validation/diff_mtm_right_{assembly_name}.png')

    plt.figure()
    im_hl = plt.imshow(S_hl[:, 2:], interpolation='nearest', aspect='auto', vmin=-6, vmax=6)
    plt.xticks(range(len(parts) - 2), parts[2:], rotation=80)
    plt.tick_params(axis='both', which='major', labelsize=10, labelbottom=False,
                    bottom=False, top=False, labeltop=True)
    plt.ylabel('time steps')
    values = list(range(7))
    colors = [im_hl.cmap(im_hl.norm(value)) for value in values]
    patches = [matplot_patches.Patch(color=colors[i], label=f"{mtm_states[i]}")
               for i in range(len(values))]
    plt.legend(handles=patches, bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
    plt.grid(True)
    plt.savefig(f'../doc/validation/mtm_module_left_{assembly_name}.png')

    plt.figure()
    im_hr = plt.imshow(S_hr[:, 2:], interpolation='nearest', aspect='auto', vmin=-6, vmax=6)
    plt.xticks(range(len(parts) - 2), parts[2:], rotation=80)
    plt.tick_params(axis='both', which='major', labelsize=10, labelbottom=False,
                    bottom=False, top=False, labeltop=True)
    plt.ylabel('time steps')
    values = list(range(7))
    colors = [im_hr.cmap(im_hr.norm(value)) for value in values]
    patches = [matplot_patches.Patch(color=colors[i], label=f"{mtm_states[i]}")
               for i in range(len(values))]
    plt.legend(handles=patches, bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
    plt.grid(True)
    plt.savefig(f'../doc/validation/mtm_module_right_{assembly_name}.png')
    plt.show()
