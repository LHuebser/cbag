from matplotlib import pyplot as plt
from matplotlib import gridspec
import numpy as np
import imageio
import argparse
import time
import networkx as nx
import cv2 as cv
# noinspection PyProtectedMember
from matplotlib.axes._axes import _log as matplotlib_axes_logger
import matplotlib.patches as matplot_patches

from cbag.inference import mtm
from cbag.inference import assembly as asm
from cbag.visualization import zeroing, visualize
from cbag.visualization.stl_mesh import draw_meshes, move_meshes
from cbag.visualization.visualize import get_img_from_fig
from cbag import utils

matplotlib_axes_logger.setLevel('ERROR')


def main(sheet_name, stop_row, interpol_factor, save_show, save_show_name, resolution, frames_per_second):
    poses = utils.load_from_xls(utils.locomotive_data_pth, sheet_name, stop_row,
                                utils.locomotive_data_fields, interpol_factor)
    parts = utils.locomotive_parts
    meshes = zeroing.load_from_stl()
    eps = utils.locomotive_epsilon
    mtm_ops = ['reach', 'grasp', 'move', 'position', 'de-position', 'release']
    mtm_states = ['default', 'reach', 'grasp', 'move', 'position', 'de-position', 'release']

    S_hl, S_hr, assembly_status, assembly_modules, assembly_graph = \
        mtm.inference_back_propagation(poses, parts, eps['epsilon_D_star'], eps['epsilon_V_rel_min'], eps['epsilon_V_rel_max'],
                                       eps['epsilon_dP'], eps['epsilon_V_abs'], utils.locomotive_connectivity,
                                       zero_vel_assumption=True, only_workpieces=True, unassembled_start_assumption=True)
    swimlanes_left = mtm.mtm_graph_from_states(S_hl)
    swimlanes_right = mtm.mtm_graph_from_states(S_hr)

    v_max = len(parts)

    instructions = asm.generate_simple_instructions(assembly_status, assembly_graph)
    node_positions = visualize.node_position_workpiece_graph(assembly_graph)

    fig = plt.figure()
    fig.tight_layout()
    gs = gridspec.GridSpec(6, 8, left=0.1, right=0.9, top=0.75, bottom=0.05, wspace=0.4, hspace=0.8)
    ax0 = fig.add_subplot(gs[:, 0])
    ax1 = fig.add_subplot(gs[:, 1])
    ax2 = fig.add_subplot(gs[:4, 2:6], projection='3d')
    ax3 = fig.add_subplot(gs[4:, 2:6])
    ax4 = fig.add_subplot(gs[:, 6])
    ax5 = fig.add_subplot(gs[:, 7])
    ax0.axis('off')
    ax1.axis('off')
    ax3.axis('off')
    ax1.set_yticklabels([])
    ax5.set_yticklabels([])
    ax = [ax0, ax1, ax2, ax3, ax4, ax5]

    ax_extension = []
    for axis in ax:
        ax_extension.append([axis.get_window_extent().x0, axis.get_window_extent().width])
    inv = fig.transFigure.inverted()
    width_hands = ax_extension[0][0] + (ax_extension[1][0] + ax_extension[1][1] - ax_extension[0][0]) / 2.
    center_hands = inv.transform((width_hands, 1))
    width_parts = ax_extension[-1][0] + (ax_extension[-2][0] + ax_extension[-2][1] - ax_extension[-1][0]) / 2.
    center_parts = inv.transform((width_parts, 1))

    fig.canvas.manager.full_screen_toggle()

    plt.ion()
    plt.show()
    plt.suptitle('Inference of MTM Operations', fontweight='bold', fontsize=15)
    plt.figtext(center_hands[0], 0.9, "MTM operations as process swimlanes", va="center", ha="center",
                fontweight='bold', size=12)
    plt.figtext(center_parts[0], 0.9, "MTM operations applied to parts (color coded)", va="center", ha="center",
                fontweight='bold', size=12)

    imgs = []
    draw_nodes = list()

    for t_step in range(poses.shape[0]):
        meshes = move_meshes(meshes, parts, poses, t_step)

        plt.sca(ax[2])
        draw_meshes(meshes, ax[2])
        ax[2].set_xlim3d(-400, 300)
        ax[2].set_ylim3d(-400, 300)
        ax[2].set_zlim3d(-10, 500)
        max_range = 700
        Xb = 0.5 * max_range * np.mgrid[-1:2:2, -1:2:2, -1:2:2][0].flatten() + 0.5 * (300 + -400)
        Yb = 0.5 * max_range * np.mgrid[-1:2:2, -1:2:2, -1:2:2][1].flatten() + 0.5 * (300 + -400)
        Zb = 0.5 * max_range * np.mgrid[-1:2:2, -1:2:2, -1:2:2][2].flatten() + 0.5 * (500 + -10)
        for xb, yb, zb in zip(Xb, Yb, Zb):
            ax[0].plot([xb], [yb], [zb], 'w')
        plt.title('Synthetic assembly scene', fontweight='bold', fontsize=12)

        plt.sca(ax[3])
        instructions_at_t = instructions[t_step]
        if instructions_at_t:
            draw_nodes = list()
            for instr in instructions_at_t:
                instr = instr.replace('assemble ', '')
                instr = instr.replace('to ', '')
                instr = instr.replace('disassemble ', '')
                instr = instr.replace('from ', '')
                draw_nodes.extend(instr.split(' '))
        residual_nodes = [node for node in list(assembly_graph.nodes()) if node not in draw_nodes]
        nx.draw(assembly_graph, pos=node_positions, nodelist=residual_nodes, node_color=(0.82, 0.83, 0.84))
        nx.draw(assembly_graph, pos=node_positions, nodelist=draw_nodes, node_color=(1, 0.549, 0), with_labels=True)
        plt.title('Assembly module graph', fontweight='bold', fontsize=12)

        plt.sca(ax[0])
        ax[0].imshow(np.concatenate((swimlanes_left[:t_step],
                                     np.zeros((swimlanes_left.shape[0] - t_step, swimlanes_left.shape[1]))), axis=0),
                     cmap='jet', interpolation='nearest', aspect='auto', vmin=0, vmax=v_max)
        plt.xticks(range(len(mtm_ops)), mtm_ops, rotation=80)
        plt.tick_params(axis='both', which='major', labelsize=10, labelbottom=False,
                        bottom=False, top=False, labeltop=True)
        plt.ylabel('time steps')
        plt.xlabel('MTM base operations')
        plt.title('Left Hand', fontweight='bold', fontsize=12)
        plt.grid(True)

        plt.sca(ax[1])
        ax[1].imshow(np.concatenate((swimlanes_right[:t_step],
                                     np.zeros((swimlanes_right.shape[0] - t_step, swimlanes_right.shape[1]))), axis=0),
                     cmap='jet', interpolation='nearest', aspect='auto', vmin=0, vmax=v_max)
        plt.xticks(range(len(mtm_ops)), mtm_ops, rotation=80)
        plt.tick_params(axis='both', which='major', labelsize=10, labelbottom=False,
                        bottom=False, top=False, labeltop=True)
        plt.xlabel('MTM base operations')
        plt.title('Right Hand', fontweight='bold', fontsize=12)
        plt.grid(True)
        ax[1].set_yticklabels([])

        plt.sca(ax[4])
        ax[4].imshow(np.concatenate((S_hl[:t_step, 2:],
                                     np.zeros((S_hl.shape[0] - t_step, S_hl.shape[1]-2))), axis=0),
                     cmap='jet', interpolation='nearest', aspect='auto', vmin=0, vmax=v_max)
        plt.xticks(range(len(parts)-2), parts[2:], rotation=80)
        plt.tick_params(axis='both', which='major', labelsize=10, labelbottom=False,
                        bottom=False, top=False, labeltop=True)
        plt.ylabel('time steps')
        plt.xlabel('assembly parts')
        plt.title('Left Hand', fontweight='bold', fontsize=12)
        plt.grid(True)

        plt.sca(ax[5])
        im_hr = ax[5].imshow(np.concatenate((S_hr[:t_step, 2:],
                                             np.zeros((S_hr.shape[0] - t_step, S_hr.shape[1]-2))), axis=0),
                             cmap='jet', interpolation='nearest', aspect='auto', vmin=0, vmax=v_max)
        plt.xticks(range(len(parts)-2), parts[2:], rotation=80)
        plt.tick_params(axis='both', which='major', labelsize=10, labelbottom=False,
                        bottom=False, top=False, labeltop=True)
        plt.xlabel('assembly parts')
        plt.title('Right Hand', fontweight='bold', fontsize=12)
        values = list(range(7))
        colors = [im_hr.cmap(im_hr.norm(value)) for value in values]
        patches = [matplot_patches.Patch(color=colors[i], label="{}".format(mtm_states[i]))
                   for i in range(len(values))]
        plt.legend(handles=patches, bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.grid(True)

        if save_show:
            img = get_img_from_fig(fig, resolution)
        else:
            img = None
        imgs.append(img)

        time.sleep(0.01)
        plt.draw()
        plt.pause(0.01)
        ax[0].cla()
        ax[1].cla()
        ax[2].cla()
    time.sleep(0.2)

    for _ in range(5):
        # noinspection PyUnboundLocalVariable
        imgs.append(img)

    if save_show:
        shape_out = np.argmax([i.shape[0] for i in imgs])
        shape_out = imgs[int(shape_out)].shape
        # hard coded fix...
        if (shape_out[0] == 2988) and (shape_out[1] == 5529):
            shape_out = (2992, 5536, 3)
        writer = imageio.get_writer('../doc/{}.mp4'.format(save_show_name), fps=frames_per_second)
        for im in imgs:
            im = cv.resize(im, (shape_out[1], shape_out[0]))
            writer.append_data(im)
        writer.close()


if __name__ == "__main__":
    # sheet:    Montage1  Montage2    Montage3    Montage4  Montage6   Montage7
    # stop row: 70        45          50          55        63         55
    parser = argparse.ArgumentParser()
    parser.add_argument('-sn', '--sheet_name', action='store', type=str, default='Montage7')
    parser.add_argument('-sr', '--stop_row', action='store', type=int, default=55)
    parser.add_argument('-if', '--interpol_factor', action='store', type=int, default=1)
    parser.add_argument('-sv', '--save_show', action='store_true', default=True)
    parser.add_argument('-svn', '--save_show_name', action='store', type=str, default='cornelius_mtm_7')
    parser.add_argument('-r', '--resolution', action='store', type=int, default=180)
    parser.add_argument('-fps', '--frames_per_second', action='store', type=int, default=3)

    args = parser.parse_args()

    main(args.sheet_name, args.stop_row, args.interpol_factor, args.save_show, args.save_show_name, args.resolution,
         args.frames_per_second)
