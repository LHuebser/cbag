# Korrelationsbasierte Erkennung von Montagereihenfolgen mittels 6 DoF-Zeitreihendaten zur Prozessdiagrammerstellung
(correlation based recognition of assembly sequences using 6 DoF timesries data for generating process graphs - cbag)

## Abstract
Current market developments cause a continuous increase in product variance and a related increased product complexity. In order to be able to react more flexibly to sales developments, batch sizes in production are reduced and faster product line changes are aimed for. As a result, an economic use of automation technologies is often not possible, so that manual assembly activities will continue to play a central role in production in the future. In combination with the emergence of iterative product development processes, assembly planning is facing new challenges that require data-driven synchronization in order to feed back findings from assembly planning to product development at an early stage. In addition, more complex assembly processes require better support for employees on the shop floor.
The recognition of process steps of manual assembly activities using algorithms offers the potential to make process knowledge available for assembly planning and product development at an early stage, to recognize process deviations during assembly and to provide process-specific support.
Based on time-series data of the poses of the components to be assembled and the hands of the assembler, related to a global reference coordinate system, an algorithm for the recognition of MTM-like process steps as well as the time course of the assembly group formation of the components is presented. Based on a transition model for assembly operations, temporal correlations are used to detect the entire process sequence. The algorithmic approach is evaluated on the basis of synthetically generated assembly data and achieves an accuracy of 91.79 % with respect to the recognition of the time sequence of individual assembly operations. In contrast to previous approaches, the newly developed algorithm enables assembly processes to be mapped in conformity with MTM.

Translated with www.DeepL.com/Translator (free version)

## Structure of this Repository
- cbag<br/>
    |- core.py......................................basic calculation used throughout the repo, e.g. velocity calculation<br/>
    |- utils.py.....................................helper and convenience functions such as loading files<br/>
    |- inference<br/>
    ....|- assembly.py..............................inference code for recognition of assembly modules<br/>
    ....|- mtm.py...................................inference code for recognition of mtm operations<br/>
    |- visualization<br/>
    ....|- stl_mesh.py..............................manipulate stl meshes for visualization<br/>
    ....|- visualize.py.............................helper functions for visualization and drawing<br/>
    ....|- zeroing.py...............................aligns coordinate frame of stl files to coordinate frame of data generation routine<br/>
- data<br/>
    |- stl..........................................all stl raw files<br/>
    ....|- base_plate.stl<br/>
    ....|- boiler.stl<br/>
    ....|- ...<br/>
    |- synth_assembly.xlsx..........................synthetic data of generation of assembly sequences<br/>
- doc<br/>
    |- scene_annotation<br/>
    ....|- assembly_1_mtm_left.csv..................annotation of mtm operations per frame in csv format<br/>
    ....|- assembly_1_mtm_left.npy..................annotation of mtm operations per frame in npy format<br/>
    ....|- assembly_1_mtm_right.csv<br/>
    ....|- assembly_1_mtm_right.npy<br/>
    ....|- assembly_2_mtm_left.csv<br/>
    ....|- assembly_2_mtm_left.npy<br/>
    ....|- ...<br/>
    |- scene_visualization<br/>
    ....|- assembly_1.mp4...........................mp4 file showing assembly scene of synthetic generated data<br/>
    ....|- assembly_2.mp4<br/>
    ....|- ...<br/>
    ....|- assembly_5.mp4<br/>
    ....|- assembly_1_10.png........................png file of assembly sequence 1 at timestep 10 (3D visualization of stl meshes)<br/>
    ....|- assembly_1_20.png<br/>
    ....|- ...<br/>
    ....|- assembly_1_50.png<br/>
    |- validation<br/>
    ....|- diff_mtm_left_assembly_1.png........shows diff between prediction and validation data of assembly scene 1 (mtm operations of left hand)<br/>
    ....|- ...<br/>
    ....|- diff_mtm_right_assembly_1.png......shows diff between prediction and validation data of assembly scene 1 (mtm operations of right hand)<br/>
    ....|- ...<br/>
    ....|- module_assembly_1.png....................shows module graph of assembly scene 1<br/>
    ....|- ...<br/>
    ....|- mtm_left_assembly_1.png..................shows mtm swimlane of left hand regarding assembly scene 1 (operations only)<br/>
    ....|- ...<br/>
    ....|- mtm_module_left_assembly_1.png...........shows mtm swimlane of left hand regarding assembly scene 1 (applied to parts)<br/>
    ....|- ...<br/>
    ....|- mtm_module_right_assembly_1.png..........shows mtm swimlane of right hand regarding assembly scene 1 (applied to parts)<br/>
    ....|- ...<br/>
    ....|- mtm_right_assembly_1.png.................shows mtm swimlane of right hand regarding assembly scene 1 (operations only)<br/>
    ....|- ...<br/>
    ....|- validation_assembly_status_assembly_1.png...shows validation data of module formation as swimlane of assembly scene 1<br/>
    ....|- ...<br/>
    ....|- validation_module_assembly_1.png.........show validation data of module graph regarding assembly scene 1<br/>
    ....|- ...<br/>
    ....|- validation_mtm_left_assembly_1.png...show validation data of mtm siwmlane of left hand regarding assembly scene 1 (operations only)<br/>
    ....|- ...<br/>
    ....|- validation_mtm_right_assembly_1.png...show validation data of mtm siwmlane of left hand regarding assembly scene 1 (operations only)<br/>
    ....|- ...<br/>
    |- cornelius_adjacency_4.mp4<br/>
    |- cornelius_module_graph_2.mp4<br/>
    |- ... various mp4 files<br/>
- scripts<br/>
    |- annotator_mtm_left.py........................annotate each frame with key action: {0: 'D', 1: 'R', 2: 'G', 3: 'M', 4: 'P', 5: 'DP', 6: 'RL'}<br/>
    |- annotator_mtm_right.py.......................annotate each frame with key action: {0: 'D', 1: 'R', 2: 'G', 3: 'M', 4: 'P', 5: 'DP', 6: 'RL'}<br/>
    |- assembly_adjacency_matrices.py...............create mp4 file showing how internal adjacency matrix evolves during recognition<br/>
    |- module_graph.py..............................create mp4 file showing course of assembly module formation from single parts<br/>
    |- mtm_swimlane.py..............................create mp4 file showing course of mtm assembly operations<br/>
    |- synth_assembly_scene.py......................create mp4 file showing 3D visualization of stl file (assembly scene)<br/>
    |- validation_data_csv_to_npy.py................convert csv file of manual annotation (annotator_mtm_left/right) to npy file<br/>
    |- validation_modules.py........................create png files showing validation data of module graphs<br/>
    |- validation_mtm.py............................create png files showing validation data of mtm swimlanes<br/>
- README.md<br/>

## Requirements
- cv2
- matplotlib
- numpy-stl
- numpy
- networkx
- pathlib
- scipy
- pandas
- imageio
- argparse

## Todos
- add argparse to scripts (just beautification)

## Note
__This Repo is currently under work in progress and only published for review purpose__
