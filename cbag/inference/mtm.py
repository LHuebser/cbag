import numpy as np

from cbag import core
from cbag.inference import assembly as asm


# noinspection PyPep8Naming
def mtm_move_step(pose, dist_mat, rel_velocity, velocity, pose_moved, hand,
                  epsilon_V_rel_min, epsilon_V_rel_max, epsilon_V_abs,
                  dest=None):
    # todo: moving with both hands gripped?!
    nd_identity_inverted = ~core.nd_identity_bool(*rel_velocity.shape)

    if hand == 'left':
        M_h = (((rel_velocity[:, 0, :] >= epsilon_V_rel_min) & (rel_velocity[:, 0, :] <= epsilon_V_rel_max)) &
               nd_identity_inverted[:, 0, :] &
               (dist_mat[:, 0, :] < dist_mat[:, 1, :]) &
               pose_moved[:, 0, :] &
               (np.bitwise_or.reduce(velocity > epsilon_V_abs, axis=1) |
                np.bitwise_or.reduce(velocity < -epsilon_V_abs, axis=1)))
        M_h[:, 1] = 0
    elif hand == 'right':
        M_h = (((rel_velocity[:, 1, :] >= epsilon_V_rel_min) & (rel_velocity[:, 1, :] <= epsilon_V_rel_max)) &
               nd_identity_inverted[:, 1, :] &
               (dist_mat[:, 1, :] < dist_mat[:, 0, :]) &
               pose_moved[:, 1, :] &
               (np.bitwise_or.reduce(velocity > epsilon_V_abs, axis=1) |
                np.bitwise_or.reduce(velocity < -epsilon_V_abs, axis=1)))
        M_h[:, 0] = 0
    else:
        raise ValueError('Argument hand must be str{left, right}!')

    if dest is not None:
        dest[M_h == 1] = 3
        S_h = dest
    else:
        S_h = np.zeros((pose.shape[0], pose.shape[2]))
        S_h[M_h == 1] = 3

    return S_h


# noinspection PyPep8Naming
def mtm_grip_move_transition_backstep(dist_mat, rel_velocity, velocity, S_h, hand,
                                      epsilon_V_rel_min, epsilon_V_rel_max, epsilon_V_abs):
    nd_identity_inverted = ~core.nd_identity_bool(*rel_velocity.shape)
    S_h_desc = np.roll(S_h, -1, axis=0)

    if hand == 'left':
        G_h = (((rel_velocity[:, 0, :] >= epsilon_V_rel_min) & (rel_velocity[:, 0, :] <= epsilon_V_rel_max)) &
               (np.bitwise_and.reduce(velocity < epsilon_V_abs, axis=1) &
                np.bitwise_and.reduce(velocity > -epsilon_V_abs, axis=1)) &
               (dist_mat[:, 0, :] < dist_mat[:, 1, :]) &
               (S_h_desc == 3) &
               nd_identity_inverted[:, 0, :])

    elif hand == 'right':
        G_h = (((rel_velocity[:, 1, :] >= epsilon_V_rel_min) & (rel_velocity[:, 1, :] <= epsilon_V_rel_max)) &
               (np.bitwise_and.reduce(velocity < epsilon_V_abs, axis=1) &
                np.bitwise_and.reduce(velocity > -epsilon_V_abs, axis=1)) &
               (dist_mat[:, 1, :] < dist_mat[:, 0, :]) &
               (S_h_desc == 3) &
               nd_identity_inverted[:, 1, :])
    else:
        raise ValueError('Argument hand must be str{left, right}!')

    S_h[G_h == 1] = 2

    return S_h


# noinspection PyPep8Naming
def mtm_position_step(rel_velocity, S_A, adj_S_A, S_h, hand, epsilon_V_rel_min, epsilon_V_rel_max,
                      unassembled_start_assumption=True):
    nd_identity_inverted = ~core.nd_identity_bool(*rel_velocity.shape)
    S_A_predesc = np.roll(S_A, 1, axis=0)

    if hand == 'left':
        P_h = (((S_A != S_A_predesc) &
                ((rel_velocity[:, 0, :] >= epsilon_V_rel_min) & (rel_velocity[:, 0, :] <= epsilon_V_rel_max)) &
                nd_identity_inverted[:, 0, :]) *
               (np.reshape(np.roll(adj_S_A, 1, 0).sum(-1).sum(-1) < adj_S_A.sum(-1).sum(-1), (S_A.shape[0], 1))))

    elif hand == 'right':
        P_h = (((S_A != S_A_predesc) &
                ((rel_velocity[:, 1, :] >= epsilon_V_rel_min) & (rel_velocity[:, 1, :] <= epsilon_V_rel_max)) &
                nd_identity_inverted[:, 1, :]) *
               (np.reshape(np.roll(adj_S_A, 1, 0).sum(-1).sum(-1) < adj_S_A.sum(-1).sum(-1), (S_A.shape[0], 1))))
    else:
        raise ValueError('Argument hand must be str{left, right}!')

    if unassembled_start_assumption:
        P_h[0, :] = False

    S_h[(P_h == 1) & (S_h != 2)] = 4
    return S_h


# noinspection PyPep8Naming
def mtm_deposition_step(rel_velocity, S_A, adj_S_A, S_h, hand, epsilon_V_rel_min, epsilon_V_rel_max,
                        unassembled_start_assumption=True):
    nd_identity_inverted = ~core.nd_identity_bool(*rel_velocity.shape)

    S_A_predesc = np.roll(S_A, 1, axis=0)

    if hand == 'left':
        P_h = (((S_A != S_A_predesc) &
                ((rel_velocity[:, 0, :] >= epsilon_V_rel_min) & (rel_velocity[:, 0, :] <= epsilon_V_rel_max)) &
                nd_identity_inverted[:, 0, :]) *
               (np.reshape(np.roll(adj_S_A, 1, 0).sum(-1).sum(-1) > adj_S_A.sum(-1).sum(-1), (S_A.shape[0], 1))))
    elif hand == 'right':
        P_h = (((S_A != S_A_predesc) &
                ((rel_velocity[:, 1, :] >= epsilon_V_rel_min) & (rel_velocity[:, 1, :] <= epsilon_V_rel_max)) &
                nd_identity_inverted[:, 1, :]) *
               (np.reshape(np.roll(adj_S_A, 1, 0).sum(-1).sum(-1) > adj_S_A.sum(-1).sum(-1), (S_A.shape[0], 1))))
    else:
        raise ValueError('Argument hand must be str{left, right}!')

    if unassembled_start_assumption:
        P_h[0, :] = False

    S_h[(P_h == 1) & (S_h != 2)] = 5
    return S_h


# noinspection PyPep8Naming
def mtm_grip_discovery_backstep(dist_mat, rel_velocity, S_h, hand,
                                epsilon_V_rel_min, epsilon_V_rel_max, epsilon_D_star):
    nd_identity_inverted = ~core.nd_identity_bool(*rel_velocity.shape)

    if hand == 'left':
        dist_grip_from_P = np.transpose(np.nonzero(S_h == 4))
        dist_grip_from_P = dist_mat[dist_grip_from_P[:, 0], 0, dist_grip_from_P[:, 1]]

        dist_grip_from_M = np.transpose(np.nonzero(S_h == 3))
        dist_grip_from_M = dist_mat[dist_grip_from_M[:, 0], 0, dist_grip_from_M[:, 1]]

        dist_grip_from_DP = np.transpose(np.nonzero(S_h == 5))
        dist_grip_from_DP = dist_mat[dist_grip_from_DP[:, 0], 0, dist_grip_from_DP[:, 1]]

        dist_weights = ([1. for _ in range(len(dist_grip_from_P))] +
                        [2. for _ in range(len(dist_grip_from_M))] +
                        [0.5 for _ in range(len(dist_grip_from_DP))])
        dist_grip = np.average(np.concatenate([dist_grip_from_P, dist_grip_from_M, dist_grip_from_DP], axis=0),
                               weights=dist_weights)

        G_h = ((dist_mat[:, 0, :] <= dist_grip * (1 + epsilon_D_star)) &
               ((rel_velocity[:, 0, :] >= epsilon_V_rel_min) & (rel_velocity[:, 0, :] <= epsilon_V_rel_max)) &
               nd_identity_inverted[:, 0, :])
    elif hand == 'right':
        dist_grip_from_P = np.transpose(np.nonzero(S_h == 4))
        dist_grip_from_P = dist_mat[dist_grip_from_P[:, 0], 1, dist_grip_from_P[:, 1]]

        dist_grip_from_M = np.transpose(np.nonzero(S_h == 3))
        dist_grip_from_M = dist_mat[dist_grip_from_M[:, 0], 1, dist_grip_from_M[:, 1]]

        dist_grip_from_DP = np.transpose(np.nonzero(S_h == 5))
        dist_grip_from_DP = dist_mat[dist_grip_from_DP[:, 0], 1, dist_grip_from_DP[:, 1]]

        dist_weights = ([1. for _ in range(len(dist_grip_from_P))] +
                        [2. for _ in range(len(dist_grip_from_M))] +
                        [0.5 for _ in range(len(dist_grip_from_DP))])
        dist_grip = np.average(np.concatenate([dist_grip_from_P, dist_grip_from_M, dist_grip_from_DP], axis=0),
                               weights=dist_weights)

        G_h = ((dist_mat[:, 1, :] <= dist_grip * (1 + epsilon_D_star)) &
               ((rel_velocity[:, 1, :] >= epsilon_V_rel_min) & (rel_velocity[:, 1, :] <= epsilon_V_rel_max)) &
               nd_identity_inverted[:, 1, :])
    else:
        raise ValueError('Argument hand must be str{left, right}!')

    G_h[:, 0] = 0
    G_h[:, 1] = 0
    S_h[(G_h == 1) & (S_h != 4) & (S_h != 3) & (S_h != 5)] = 2

    return S_h, dist_grip


# noinspection PyPep8Naming
def mtm_release_step(rel_velocity, S_h, hand, epsilon_V_rel_max, unassembled_start_assumption,
                     dist_mat=None, d_star_grip=None):
    S_h_predesc = np.roll(S_h, 1, axis=0)

    if hand == 'left':
        if dist_mat is not None and d_star_grip is not None:
            RL_h = (((S_h_predesc == 2) | (S_h_predesc == 3) | (S_h_predesc == 4) | (S_h_predesc == 5)) &
                    (rel_velocity[:, 0, :] > epsilon_V_rel_max) &
                    (dist_mat[:, 0, :] > d_star_grip))
        else:
            RL_h = (((S_h_predesc == 2) | (S_h_predesc == 3) | (S_h_predesc == 4) | (S_h_predesc == 5)) &
                    (rel_velocity[:, 0, :] > epsilon_V_rel_max))

    elif hand == 'right':
        if dist_mat is not None and d_star_grip is not None:
            RL_h = (((S_h_predesc == 2) | (S_h_predesc == 3) | (S_h_predesc == 4) | (S_h_predesc == 5)) &
                    (rel_velocity[:, 1, :] > epsilon_V_rel_max) &
                    (dist_mat[:, 1, :] > d_star_grip))
        else:
            RL_h = (((S_h_predesc == 2) | (S_h_predesc == 3) | (S_h_predesc == 4) | (S_h_predesc == 5)) &
                    (rel_velocity[:, 1, :] > epsilon_V_rel_max))
    else:
        raise ValueError('Argument hand must be str{left, right}!')

    if unassembled_start_assumption:
        RL_h[0, :] = 0

    RL_h[:, 0] = 0
    RL_h[:, 1] = 0

    S_h[(RL_h == 1) & (S_h != 2) & (S_h != 3) & (S_h != 4) & (S_h != 5)] = 6
    return S_h


# noinspection PyPep8Naming
def mtm_reach_step(velocity, S_h, hand, epsilon_V_abs):
    if hand == 'left':
        R_h = (((np.bitwise_or.reduce(velocity[:, :, 0] > epsilon_V_abs, axis=1) |
                 np.bitwise_or.reduce(velocity[:, :, 0] < -epsilon_V_abs, axis=1)).reshape((-1, 1)) *
                np.bitwise_and.reduce(S_h == 0, axis=1).reshape((-1, 1)) * np.ones((1, 10))).astype(np.bool))

    elif hand == 'right':
        R_h = (((np.bitwise_or.reduce(velocity[:, :, 1] > epsilon_V_abs, axis=1) |
                 np.bitwise_or.reduce(velocity[:, :, 1] < -epsilon_V_abs, axis=1)).reshape((-1, 1)) *
                np.bitwise_and.reduce(S_h == 0, axis=1).reshape((-1, 1)) * np.ones((1, 10))).astype(np.bool))
    else:
        raise ValueError('Argument hand must be str{left, right}!')

    S_h[(R_h == 1) & (S_h == 0)] = 1
    return S_h


# noinspection PyPep8Naming
def mtm_clean_reach_backstep(S_h):
    for i in range(S_h.shape[0] - 1, 0, -1):
        if 1 in S_h[i, :]:
            non_zero_ind_per_col = core.first_non_zero(S_h[i + 1:, :] != 0, 0, S_h.shape[0] - 2 - i)
            non_zero_vals = [S_h[i + 1 + c, e] for e, c in enumerate(non_zero_ind_per_col)]
            for j, compare_val in enumerate(non_zero_vals):
                if (S_h[i, j] == 1) and (S_h[i + 1, j] != 1) and compare_val != 2 and np.argmin(non_zero_ind_per_col) != j:
                    S_h[i, j] = 0
    return S_h


# noinspection PyPep8Naming
def mtm_clean_reach_backstep_paper_version(S_h):
    for t in range(S_h.shape[0]):
        for i in range(S_h.shape[1]):
            # find first entry equal 1
            r = t
            while r < S_h.shape[0] and S_h[r, i] != 1:
                r += 1

            n = r
            while n < S_h.shape[0]:
                if S_h[n, i] != 1 and n != (S_h.shape[0]-1):
                    if S_h[n, i] == 2:
                        break
                    k = r
                    while k <= max(n - 1, r):
                        S_h[k, i] = 0
                        k += 1
                    break
                n += 1
    return S_h


# noinspection PyPep8Naming
def inference_back_propagation(pose, parts, epsilon_D_star, epsilon_V_rel_min, epsilon_V_rel_max, epsilon_dP, epsilon_V_abs,
                               connectivity=None, zero_vel_assumption=True, only_workpieces=True, unassembled_start_assumption=True):
    D_star = core.distance_matrix(np.array([pose[-1]]))
    D_star = D_star[-1, :, :]
    if connectivity is not None:
        D_star = asm.assembly_connectivity(D_star, connectivity)

    assembly_status, assembly_modules, assembly_graph, D, V_rel, P_moved, S_adj = \
        asm.inference_step(pose, parts, D_star, epsilon_D_star, epsilon_V_rel_min, epsilon_V_rel_max, epsilon_dP,
                           zero_vel_assumption, only_workpieces=only_workpieces)

    V = core.velocity(pose)

    S_hl = mtm_move_step(pose, D, V_rel, V, P_moved, 'left', epsilon_V_rel_min, epsilon_V_rel_max, epsilon_V_abs)
    S_hr = mtm_move_step(pose, D, V_rel, V, P_moved, 'right', epsilon_V_rel_min, epsilon_V_rel_max, epsilon_V_abs)

    S_hl = mtm_grip_move_transition_backstep(D, V_rel, V, S_hl, 'left',
                                             epsilon_V_rel_min, epsilon_V_rel_max, epsilon_V_abs)
    S_hr = mtm_grip_move_transition_backstep(D, V_rel, V, S_hr, 'right',
                                             epsilon_V_rel_min, epsilon_V_rel_max, epsilon_V_abs)

    S_hl = mtm_position_step(V_rel, assembly_status, S_adj, S_hl, 'left', epsilon_V_rel_min, epsilon_V_rel_max,
                             unassembled_start_assumption)
    S_hr = mtm_position_step(V_rel, assembly_status, S_adj, S_hr, 'right', epsilon_V_rel_min, epsilon_V_rel_max,
                             unassembled_start_assumption)

    S_hl = mtm_deposition_step(V_rel, assembly_status, S_adj, S_hl, 'left',
                               epsilon_V_rel_min, epsilon_V_rel_max, unassembled_start_assumption)
    S_hr = mtm_deposition_step(V_rel, assembly_status, S_adj, S_hr, 'right',
                               epsilon_V_rel_min, epsilon_V_rel_max, unassembled_start_assumption)

    S_hl, dist_grip_hl = mtm_grip_discovery_backstep(D, V_rel, S_hl, 'left', epsilon_V_rel_min, epsilon_V_rel_max,
                                                     epsilon_D_star)
    S_hr, dist_grip_hr = mtm_grip_discovery_backstep(D, V_rel, S_hr, 'right', epsilon_V_rel_min, epsilon_V_rel_max,
                                                     epsilon_D_star)

    S_hl = mtm_release_step(V_rel, S_hl, 'left', epsilon_V_rel_max, unassembled_start_assumption, D, dist_grip_hl)
    S_hr = mtm_release_step(V_rel, S_hr, 'right', epsilon_V_rel_max, unassembled_start_assumption, D, dist_grip_hr)

    S_hl = mtm_reach_step(V, S_hl, 'left', epsilon_V_abs)
    S_hr = mtm_reach_step(V, S_hr, 'right', epsilon_V_abs)

    # S_hl = mtm_clean_reach_backstep(S_hl)
    # S_hr = mtm_clean_reach_backstep(S_hr)
    S_hl = mtm_clean_reach_backstep_paper_version(S_hl)
    S_hr = mtm_clean_reach_backstep_paper_version(S_hr)

    return S_hl, S_hr, assembly_status, assembly_modules, assembly_graph


# noinspection PyPep8Naming
def mtm_graph_from_states(S_h):
    mtm_G = np.zeros((S_h.shape[0], 6))
    idx = np.transpose(np.nonzero(S_h != 0))
    idx_i = idx[:, 0]
    idx_j = (S_h[idx[:, 0], idx[:, 1]]-1).astype(np.int)

    mtm_G[idx_i, idx_j] = 1

    mtm_G *= np.array([[1, 2, 3, 4, 5, 6] for _ in range(S_h.shape[0])])

    return mtm_G


if __name__ == "__main__":
    from cbag import utils
    from cbag.visualization import visualize as vis
    from matplotlib import pyplot as plt

    P = utils.load_from_xls(utils.locomotive_data_pth, 'Montage7', 55, utils.locomotive_data_fields, 3)

    parts = utils.locomotive_parts

    epsilon_D_star = 0.01  # percentage, constraint relaxation
    epsilon_V_rel_max = 10  # fluctuation in mm per time step, constraint relaxation
    epsilon_V_rel_min = -45
    epsilon_dP = 0.000001  # deviation accountable to measurement uncertainty, constraint tightening
    epsilon_V_abs = 5

    S_hl, S_hr, assembly_status, assembly_modules, assembly_graph = \
        inference_back_propagation(P, parts, epsilon_D_star, epsilon_V_rel_min, epsilon_V_rel_max, epsilon_dP, epsilon_V_abs,
                                   utils.locomotive_connectivity)

    G = mtm_graph_from_states(S_hl)

    merged = np.concatenate((S_hl, np.zeros((S_hl.shape[0], 1)), assembly_status, np.zeros((S_hl.shape[0], 1)), S_hr),
                            axis=1)

    #import pickle
    #with open('../doc/assembly_status_6.pickle', 'wb') as handle:
        #pickle.dump(merged, handle, protocol=pickle.HIGHEST_PROTOCOL)

    plt.figure()
    plt.imshow(merged, cmap='jet')
    plt.figure()
    plt.imshow(G, cmap='jet')
    plt.show()
