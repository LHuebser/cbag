import numpy as np
import networkx as nx

from cbag import core


# noinspection PyPep8Naming
def adjacency_state_space(dist_mat, vel_rel, pose_has_moved, D_star,
                          eps_v_rel_min, eps_v_rel_max, eps_d, only_workpieces=True):
    nd_identity_inverted = ~core.nd_identity_bool(*dist_mat.shape)
    # check condition
    adjacency_state = ((dist_mat <= D_star * (1 + eps_d)) &
                       (vel_rel <= eps_v_rel_max) & (vel_rel >= eps_v_rel_min) &
                       nd_identity_inverted)
    # lower adj matrix indices
    i_lower = np.tril_indices(adjacency_state.shape[1])
    for i in range(adjacency_state.shape[0]):
        # replace lower matrix indices with False
        adjacency_state[i][i_lower] = False
        # pose has moved vector for comparison
        pose_has_moved_vector = np.diagonal(pose_has_moved[i])
        # loop over adj columns
        for j in range(adjacency_state[i].shape[0]):
            # check if column contains True value in adj and False value in vector
            if True in adjacency_state[i][:, j] and not pose_has_moved_vector[j]:
                # replace column values
                adjacency_state[i][:, j] = False
        # copy correct upper matrix values to lower matrix
        adjacency_state[i][i_lower] = adjacency_state[i].T[i_lower]

    if only_workpieces:
        adjacency_state[:, 0, :] = False
        adjacency_state[:, 1, :] = False
        adjacency_state[:, :, 0] = False
        adjacency_state[:, :, 1] = False
    return adjacency_state


# noinspection PyPep8Naming
def module_state_space(adj_state_space):
    state_space = np.zeros((adj_state_space.shape[0], adj_state_space.shape[1]), dtype=int)
    module_counter = 1
    modul_register = {}
    for t_step in range(adj_state_space.shape[0]):
        adj_state_t = adj_state_space[t_step, :, :]
        aux_graph = nx.from_numpy_array(adj_state_t)

        ids_state_assembled = np.transpose(np.nonzero(adj_state_t == 1))[:, 0]

        while len(ids_state_assembled) != 0:
            descendants = list(nx.descendants(aux_graph, ids_state_assembled[0]))
            descendants.append(ids_state_assembled[0])
            module_i = frozenset(descendants)
            if module_i in modul_register:
                state_space[t_step, descendants] = modul_register[module_i]
            else:
                state_space[t_step, descendants] = module_counter
                modul_register[module_i] = module_counter
                module_counter += 1
            ids_state_assembled = list(set(ids_state_assembled).difference(set(descendants)))

            if not ids_state_assembled:  # empty list so break out
                break
    modules = {v: k for k, v in modul_register.items()}
    return state_space, modules


# noinspection PyPep8Naming
def assembly_connectivity(D_star, connectivity_tuples):
    C = np.zeros_like(D_star)
    C[connectivity_tuples[:, 0], connectivity_tuples[:, 1]] = 1
    C[connectivity_tuples[:, 1], connectivity_tuples[:, 0]] = 1
    return D_star * C


# noinspection PyPep8Naming
def module_graph(module_states, part_names):
    _workpiece_graph = nx.DiGraph()
    for t_step in range(module_states.shape[0]):
        if np.sum(module_states[t_step]) <= 0:
            continue

        for _part_id in range(module_states.shape[1]):
            if t_step > 0 and module_states[t_step - 1, _part_id] == 0 and module_states[t_step, _part_id] != 0:
                _workpiece_graph.add_edge(part_names[_part_id], f'module_{module_states[t_step, _part_id]}')

            elif (t_step > 0 and module_states[t_step - 1, _part_id] != module_states[t_step, _part_id] and
                  module_states[t_step - 1, _part_id] != 0):
                if module_states[t_step, _part_id] != 0:    # assembly
                    _workpiece_graph.add_edge(f'module_{module_states[t_step - 1, _part_id]}',
                                              f'module_{module_states[t_step, _part_id]}')
                else:  # disassembly
                    _workpiece_graph.add_edge(f'module_{module_states[t_step - 1, _part_id]}',
                                              part_names[_part_id])
            elif t_step == 0 and module_states[t_step, _part_id] != 0:
                _workpiece_graph.add_edge(part_names[_part_id], f'module_{module_states[t_step, _part_id]}')
    return _workpiece_graph


def generate_simple_instructions(assembly_status, workpiece_graph):
    instructions = [[]]
    for i in range(1, assembly_status.shape[0]):
        if np.bitwise_and.reduce(assembly_status[i] == assembly_status[i-1]):
            instructions.append([])
        else:
            instr_i = []
            for j in range(assembly_status.shape[1]):
                if assembly_status[i, j] != 0:
                    node_name = f'module_{assembly_status[i, j]}'
                    incoming_nodes = [v if u == node_name else u
                                      for (u, v) in workpiece_graph.in_edges(node_name)]
                    for node in incoming_nodes:
                        instr_j = f'assemble {node} to {node_name}'
                        if instr_j not in instr_i:
                            instr_i.append(instr_j)

                    if workpiece_graph.out_degree(node_name) > 1:
                        outgoing_nodes = [v if u == node_name else u
                                          for (u, v) in workpiece_graph.out_edges(node_name)]
                        for node in outgoing_nodes:
                            instr_j = f'dissassemble {node} from {node_name}'
                            if instr_j not in instr_i:
                                instr_i.append(instr_j)
            instructions.append(instr_i)
    return instructions


# noinspection PyPep8Naming
def inference_step(pose, parts, D_star, epsilon_D_star, epsilon_V_rel_min, epsilon_V_rel_max, epsilon_dP,
                   zero_vel_assumption, only_workpieces=True):
    D = core.distance_matrix(pose)
    V_rel = core.relative_velocity(D, zero_vel_assumption)

    P_moved = core.has_changed_init_pose(pose, epsilon_dP)

    S_adj = adjacency_state_space(D, V_rel, P_moved, D_star,
                                  epsilon_V_rel_min, epsilon_V_rel_max, epsilon_D_star, only_workpieces)

    assembly_status, assembly_modules = module_state_space(S_adj)
    assembly_graph = module_graph(assembly_status, parts)

    return assembly_status, assembly_modules, assembly_graph, D, V_rel, P_moved, S_adj
