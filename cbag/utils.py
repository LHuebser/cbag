import numpy as np
from scipy import interpolate
import pandas as pd
import pathlib

locomotive_data_pth = pathlib.Path(__file__).parent / '../data/synth_assembly.xlsx'
locomotive_data_fields = ['step_id', 'hand_left', 'hand_right', 'base_plate', 'boiler', 'cab_lower', 'cab_upper',
                          'tire_1', 'tire_2', 'tire_3', 'tire_4']
locomotive_parts = locomotive_data_fields[1:]
locomotive_connectivity = np.array([[2, 3], [2, 4], [2, 6], [2, 7], [2, 8], [2, 9], [4, 5]])
locomotive_epsilon = {'epsilon_D_star': 0.01,  # percentage, constraint relaxation
                      'epsilon_V_rel_max': 10,  # fluctuation in mm per time step, constraint relaxation
                      'epsilon_V_rel_min': -45,
                      'epsilon_dP': 0.000001,  # deviation accountable to measurement uncertainty, constraint tightening
                      'epsilon_V_abs': 5}
locomotive_colors = {'base_plate': 'navy', 'boiler': 'maroon', 'cab_lower': 'coral', 'cab_upper': 'darkgreen',
                     'tire_1': 'sienna', 'tire_2': 'brown', 'tire_3': 'chocolate', 'tire_4': 'olive'}


def interpol_1d(orig_space, new_space, orig_values):
    return np.reshape(interpolate.interp1d(orig_space, orig_values, 'linear')(new_space), (-1, 1))


def load_from_xls(pth, sht_name, stop_row, data_fields, interpol_factor):
    data = pd.read_excel(pth, sheet_name=sht_name).to_numpy()

    pose = []
    for i in range(1, len(data_fields)):
        raw_data = np.array(data[3:stop_row, 2 + 6 * (i - 1):2 + 6 * i], dtype=np.float)
        x_old = np.linspace(0, len(raw_data), len(raw_data))
        x_new = np.linspace(0, len(raw_data), interpol_factor * len(raw_data))
        new_raw_data = np.concatenate([interpol_1d(x_old, x_new, raw_data[:, i]) for i in range(6)], axis=1)
        pose.append(new_raw_data)

    return np.moveaxis(np.array(pose), 0, -1)
