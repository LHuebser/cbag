import numpy as np


def compare_all_equal(x, rel_tol):
    argmax_x = np.argmax(x)
    rest_arg = list(range(len(x)))
    rest_arg.remove(argmax_x)
    bool_equal = [np.isclose(x[argmax_x], x[_i], rtol=rel_tol) for _i in rest_arg]
    return np.sum(bool_equal) == len(bool_equal)


def first_non_zero(arr, axis, invalid_replace=-1):
    mask = (arr != 0)
    return np.where(mask.any(axis=axis), mask.argmax(axis=axis), invalid_replace)


def distance_matrix(pose):
    if len(pose.shape) <= 2:
        pose = np.array([pose])

    distances = np.zeros((pose.shape[0], pose.shape[2], pose.shape[2]))
    for _i in range(pose.shape[2]):
        for _j in range(_i):
            _d = np.sqrt(np.sum((pose[:, :3, _i] - pose[:, :3, _j]) ** 2, axis=1))
            distances[:, _i, _j] = _d
            distances[:, _j, _i] = _d
    return distances


def has_changed_init_pose(pose, eps_delta_pose_init=0.01):
    # pose_has_moved = (np.bitwise_or.reduce(pose > (pose[0] * (1 + np.sign(pose[0]) * eps_delta_pose_init)), 1) |
    #                   np.bitwise_or.reduce(pose < (pose[0] * (1 - np.sign(pose[0]) * eps_delta_pose_init)), 1))
    delta = np.abs(pose - pose[0])
    pose_has_moved = np.zeros((pose.shape[0], pose.shape[2]))
    pose_has_moved[np.bitwise_or.reduce(delta > np.abs(pose[0] * eps_delta_pose_init), 1)] = 1
    pose_has_moved = np.array(pose_has_moved, dtype=np.bool)

    pose_has_moved = np.moveaxis(np.expand_dims(pose_has_moved, 2), 2, 1)
    pose_has_moved = np.repeat(pose_has_moved, pose.shape[2], axis=1)
    pose_has_moved = pose_has_moved | np.rot90(pose_has_moved, k=3, axes=(1, 2))    # | or &? --> |
    return pose_has_moved


def relative_velocity(dist_mat, zero_vel_assumption=True):
    _V_rel = np.diff(dist_mat, axis=0)
    _V_rel = np.concatenate((np.zeros((1, dist_mat.shape[2], dist_mat.shape[2])), _V_rel), axis=0)
    if zero_vel_assumption:
        _V_rel[-1] = 0
    return _V_rel


def velocity(pose, zero_vel_assumption=True):
    _V = np.diff(pose, axis=0)
    _V = np.concatenate((np.zeros((1, pose.shape[1], pose.shape[2])), _V), axis=0)
    if zero_vel_assumption:
        _V[-1] = 0
    return _V


def nd_identity_bool(t, n, m):
    identity = np.zeros((t, n, m))
    identity[:, np.arange(n), np.arange(m)] = 1
    return identity.astype(np.bool, copy=False)


if __name__ == "__main__":
    from cbag import utils
    from cbag.inference import assembly as asm
    from cbag.visualization import visualize as vis
    from matplotlib import pyplot as plt

    P = utils.load_from_xls(utils.locomotive_data_pth, 'Montage4', 55, utils.locomotive_data_fields, 3)
    parts = utils.locomotive_parts

    print(P.shape)
    D = distance_matrix(P)
    V_rel = relative_velocity(D, True)

    D_star = D[-1, :, :]
    # assembly_connectivity = np.array([[2, 3], [2, 4], [2, 6], [2, 7], [2, 8], [2, 9], [4, 5]])
    # C = np.zeros_like(D_star)
    # for i in assembly_connectivity:
    #     C[i[0], i[1]] = 1
    #     C[i[1], i[0]] = 1
    # print(C)
    # D_star *= C
    D_star = asm.assembly_connectivity(D_star, utils.locomotive_connectivity)

    epsilon_D_star = 0.01  # percentage, constraint relaxation
    epsilon_V_rel_max = 0.1  # fluctuation in mm per time step, constraint relaxation
    epsilon_V_rel_min = -0.1
    epsilon_dP = 0.000001  # deviation accountable to measurement uncertainty, constraint tightening

    P_moved = has_changed_init_pose(P, epsilon_dP)

    S_adj = asm.adjacency_state_space(D, V_rel, P_moved, D_star, epsilon_V_rel_min, epsilon_V_rel_max, epsilon_D_star,
                                      True)

    # plt.figure()
    # plt.ion()
    # plt.show()
    # for t in range(S_adj.shape[0]):
    #     plt.imshow(S_adj[t], cmap='jet')
    #     plt.xticks(range(len(parts)), parts, rotation=80)
    #     plt.tick_params(axis='both', which='major', labelsize=10, labelbottom=False, bottom=False, top=False,
    #                     labeltop=True)
    #     print(t)
    #     plt.draw()
    #     plt.pause(0.05)
    #     plt.cla()
    # exit(0)

    assembly_status, assembly_modules = asm.module_state_space(S_adj)
    assembly_graph = asm.module_graph(assembly_status, parts)

    instructions = asm.generate_simple_instructions(assembly_status, assembly_graph)
    print(instructions)

    plt.figure()
    vis.draw_workpiece_graph(assembly_graph)

    plt.figure()
    plt.imshow(assembly_status)
    plt.xticks(range(len(parts)), parts, rotation=80)
    plt.tick_params(axis='both', which='major', labelsize=10, labelbottom=False, bottom=False, top=False, labeltop=True)
    print(assembly_modules)
    plt.show()
