import numpy as np
import cv2 as cv
import io
import networkx as nx


def get_img_from_fig(fig, dpi=180):
    buf = io.BytesIO()
    fig.savefig(buf, format="png", dpi=dpi)
    buf.seek(0)
    img_arr = np.frombuffer(buf.getvalue(), dtype=np.uint8)
    buf.close()
    img = cv.imdecode(img_arr, 1)
    img = cv.cvtColor(img, cv.COLOR_BGR2RGB)

    return img


def node_position_workpiece_graph(graph):
    pos = nx.drawing.kamada_kawai_layout(graph)
    pos = nx.spring_layout(graph, pos=pos)
    return pos


def draw_workpiece_graph(graph):
    pos = node_position_workpiece_graph(graph)
    nx.draw(graph, pos=pos, with_labels=True)
