import pathlib
import numpy as np
from stl import mesh

from cbag import utils
from cbag.visualization import stl_mesh

pth_cab_lower = pathlib.Path(__file__).parent / '../../data/stl/cab_lower.stl'
pth_cab_upper = pathlib.Path(__file__).parent / '../../data/stl/cab_upper.stl'
pth_boiler = pathlib.Path(__file__).parent / '../../data/stl/boiler.stl'
pth_base_plate = pathlib.Path(__file__).parent / '../../data/stl/base_plate.stl'
pth_tire = pathlib.Path(__file__).parent / '../../data/stl/tire.stl'
pth_hand_right = pathlib.Path(__file__).parent / '../../data/stl/hand_right.stl'
pth_hand_left = pathlib.Path(__file__).parent / '../../data/stl/hand_left.stl'

moves_to_zero = {'cab_lower': {'dir': pth_cab_lower, 'rotate': [[0.5, 0, 0], np.deg2rad(-90)],
                               'translate': [-35.0, 29.0, 4.070319]},
                 'cab_upper': {'dir': pth_cab_upper, 'rotate': [[0.5, 0, 0], np.deg2rad(-90)],
                               'translate': [35., 28.72281265258789, 7.806256e-15]},
                 'boiler': {'dir': pth_boiler, 'rotate': [[0.5, 0, 0], np.deg2rad(-90)],
                            'translate': [-35.0, 25.0, 4.070319]},
                 'base_plate': {'dir': pth_base_plate, 'rotate': [[0, 0, 0], 0],
                                'translate': [-70.0, -30.0, 0.0]},
                 'tire_1': {'dir': pth_tire, 'rotate': [[0.5, 0, 0], np.deg2rad(90)],
                            'translate': [0., 0., 7.]},
                 'tire_2': {'dir': pth_tire, 'rotate': [[0.5, 0, 0], np.deg2rad(90)],
                            'translate': [0., 0., 7.]},
                 'tire_3': {'dir': pth_tire, 'rotate': [[0.5, 0, 0], np.deg2rad(90)],
                            'translate': [0., 0., 7.]},
                 'tire_4': {'dir': pth_tire, 'rotate': [[0.5, 0, 0], np.deg2rad(90)],
                            'translate': [0., 0., 7.]},
                 'hand_right': {'dir': pth_hand_right, 'rotate': [[0, 0, 0], 0],
                                'translate': [55., 0., 0.]},
                 'hand_left': {'dir': pth_hand_left, 'rotate': [[0, 0, 0], 0],
                               'translate': [-35., 0., 0.]}
                 }

parts = ['step_id', 'hand_left', 'hand_right', 'base_plate', 'boiler', 'cab_lower', 'cab_upper',
         'tire_1', 'tire_2', 'tire_3', 'tire_4']


def load_from_stl():
    meshes = {}
    for part_name in utils.locomotive_parts:
        m_ = mesh.Mesh.from_file(str(moves_to_zero[part_name]['dir'].resolve()))
        m_.rotate(moves_to_zero[part_name]['rotate'][0], moves_to_zero[part_name]['rotate'][1])
        m_ = stl_mesh.translate(m_, *moves_to_zero[part_name]['translate'])
        meshes[part_name] = m_
    m_ = meshes['boiler']
    m_.rotate([0, 0, .5], np.deg2rad(180))
    return meshes
