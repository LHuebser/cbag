import numpy as np
import stl
from mpl_toolkits import mplot3d
from cbag.utils import locomotive_colors as part_colors


def _envelope(_mesh):
    max_x = np.max([p[stl.Dimension.X] for p in _mesh.points])
    max_y = np.max([p[stl.Dimension.Y] for p in _mesh.points])
    max_z = np.max([p[stl.Dimension.Z] for p in _mesh.points])
    min_x = np.min([p[stl.Dimension.X] for p in _mesh.points])
    min_y = np.min([p[stl.Dimension.Y] for p in _mesh.points])
    min_z = np.min([p[stl.Dimension.Z] for p in _mesh.points])
    return min_x, min_y, min_z, max_x, max_y, max_z


def translate(_mesh, x, y, z):
    _mesh.x += int(x)
    _mesh.y += int(y)
    _mesh.z += int(z)
    return _mesh


def rotate(_mesh, axis_list, deg_increment):
    min_x, min_y, min_z, max_x, max_y, max_z = _envelope(_mesh)
    w_x = max_x - min_x
    w_y = max_y - min_y
    w_z = max_z - min_z
    _mesh.rotate(axis_list,
                 np.deg2rad(deg_increment),
                 np.array([min_x + w_x / 2,
                           min_y + w_y / 2,
                           min_z + w_z / 2]))
    return _mesh


def move_meshes(mesh_dict, parts, poses, t_step):
    for i in range(len(parts)):
        part_name = parts[i]
        if t_step == 0:
            old_x, old_y, old_z, old_px, old_py, old_pz = 0, 0, 0, 0, 0, 0
        else:
            old_x = poses[t_step - 1, 0, i]
            old_y = poses[t_step - 1, 1, i]
            old_z = poses[t_step - 1, 2, i]
            old_px = poses[t_step - 1, 3, i]
            old_py = poses[t_step - 1, 4, i]
            old_pz = poses[t_step - 1, 5, i]

        m_ = mesh_dict[part_name]
        m_ = translate(m_,
                       poses[t_step, 0, i] - old_x,
                       poses[t_step, 1, i] - old_y,
                       poses[t_step, 2, i] - old_z)

        m_ = rotate(m_, [0.5, 0, 0], poses[t_step, 3, i] - old_px)
        m_ = rotate(m_, [0, 0.5, 0], poses[t_step, 4, i] - old_py)
        m_ = rotate(m_, [0, 0, 0.5], poses[t_step, 5, i] - old_pz)

        mesh_dict[part_name] = m_
    return mesh_dict


# noinspection PyUnresolvedReferences
def draw_meshes(mesh_list, ax):
    for key in mesh_list:
        mesh = mesh_list[key]
        if key == 'hand_left':
            ax.add_collection3d(mplot3d.art3d.Poly3DCollection(mesh.vectors, edgecolor='k',
                                                               alpha=0.5, linewidth=0.15, color='blue'))
        elif key == 'hand_right':
            ax.add_collection3d(mplot3d.art3d.Poly3DCollection(mesh.vectors, edgecolor='k',
                                                               alpha=0.5, linewidth=0.15, color='orange'))
        else:
            ax.add_collection3d(mplot3d.art3d.Poly3DCollection(mesh.vectors, edgecolor=None,
                                                               alpha=0.65, linewidth=0.15, color=part_colors[key]))
    return ax
